package com.shchyrov.skysoft.privatbank.model

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.Query
import com.shchyrov.skysoft.privatbank.entity.Terminal
import io.reactivex.Single

@Dao
interface TerminalDao {
    @Query("SELECT * FROM terminal")
    fun getTerminals(): Single<List<Terminal>>

    @Insert
    fun insertAll(vararg terminals: Terminal)

    @Query("DELETE FROM terminal")
    fun clearAll()
}