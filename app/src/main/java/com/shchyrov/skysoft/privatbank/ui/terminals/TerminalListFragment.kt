package com.shchyrov.skysoft.privatbank.ui.terminals

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.shchyrov.skysoft.privatbank.R
import com.shchyrov.skysoft.privatbank.databinding.FragmentTerminalListBinding
import com.shchyrov.skysoft.privatbank.di.ViewModelFactory
import com.shchyrov.skysoft.privatbank.entity.Terminal
import com.shchyrov.skysoft.privatbank.ui.terminaldetails.TerminalDetailsFragment
import com.shchyrov.skysoft.privatbank.utils.extension.replace

class TerminalListFragment : Fragment() {

    private lateinit var binding: FragmentTerminalListBinding
    private lateinit var viewModel: TerminalListViewModel

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_terminal_list, container, false)

        binding.recyclerTerminals.layoutManager = LinearLayoutManager(context, RecyclerView.VERTICAL, false)

        viewModel = ViewModelProviders.of(this, ViewModelFactory(this)).get(TerminalListViewModel::class.java)

        viewModel.getSelectedTerminal().observe({ lifecycle }, { item -> item?.let { displayDetails(it) } })

        binding.viewModel = viewModel

        return binding.root
    }

    private fun displayDetails(item: Terminal) {
        Log.d("OSchirov", "replace!")
        viewModel.setSelectedTerminal(null)
        fragmentManager?.replace(
            TerminalDetailsFragment::class.java,
            R.id.container_main,
            TerminalDetailsFragment.getBundleData(item)
        )
    }
}