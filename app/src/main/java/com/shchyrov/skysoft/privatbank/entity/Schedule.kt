package com.shchyrov.skysoft.privatbank.entity

import android.os.Parcelable
import androidx.room.Entity
import androidx.room.PrimaryKey
import kotlinx.android.parcel.Parcelize

@Parcelize
class Schedule(
    @PrimaryKey(autoGenerate = true)
    val id: Int,
    val mon: String,
    val tue: String,
    val wed: String,
    val thu: String,
    val fri: String,
    val sat: String,
    val sun: String,
    val hol: String
) : Parcelable {
    override fun toString(): String {
        return "Schedule(mon='$mon', tue='$tue', wed='$wed', thu='$thu', fri='$fri', sat='$sat', sun='$sun', hol='$hol')"
    }
}