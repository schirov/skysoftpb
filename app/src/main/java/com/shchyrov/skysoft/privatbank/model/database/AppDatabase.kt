package com.shchyrov.skysoft.privatbank.model.database

import androidx.room.Database
import androidx.room.RoomDatabase
import androidx.room.TypeConverters
import com.shchyrov.skysoft.privatbank.entity.Schedule
import com.shchyrov.skysoft.privatbank.entity.Terminal
import com.shchyrov.skysoft.privatbank.model.TerminalDao

@Database(entities = [Terminal::class], version = 1, exportSchema = false)
@TypeConverters(Converters::class)
abstract class AppDatabase : RoomDatabase() {
    abstract fun terminalDao(): TerminalDao
}