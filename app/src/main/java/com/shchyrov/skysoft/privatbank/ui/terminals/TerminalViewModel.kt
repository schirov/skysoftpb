package com.shchyrov.skysoft.privatbank.ui.terminals

import androidx.lifecycle.MutableLiveData
import com.shchyrov.skysoft.privatbank.base.BaseViewModel
import com.shchyrov.skysoft.privatbank.entity.Terminal
import com.shchyrov.skysoft.privatbank.utils.listeners.OnItemClickListener

class TerminalViewModel : BaseViewModel() {
    lateinit var entity: Terminal
    val type = MutableLiveData<String>()
    val city = MutableLiveData<String>()
    val fullAddress = MutableLiveData<String>()
    val place = MutableLiveData<String>()
    val latitude = MutableLiveData<Double>()
    val longitude = MutableLiveData<Double>()

    private lateinit var listener: OnItemClickListener

    fun bind(terminal: Terminal) {
        entity = terminal
        type.value = terminal.type
        city.value = terminal.cityUA
        fullAddress.value = terminal.fullAddressUa
        place.value = terminal.placeUa
        latitude.value = terminal.latitude
        longitude.value = terminal.longitude
    }

    fun onTerminalClicked() {
        listener.onItemClick(entity)
    }

    fun setOnClickListener(listener: OnItemClickListener) {
        this.listener = listener
    }
}