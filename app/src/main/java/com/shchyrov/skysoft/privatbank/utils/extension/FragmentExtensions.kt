package com.shchyrov.skysoft.privatbank.utils.extension

import android.os.Bundle
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager

fun FragmentManager.replace(
    fragmentClass: Class<out Fragment>,
    containerId: Int,
    args: Bundle = Bundle(),
    toStack: Boolean = true
) {
    val fragment = fragmentClass.newInstance()
    fragment.arguments = args
    val transaction = beginTransaction()
        .replace(containerId, fragment)

    if (toStack) {
        transaction.addToBackStack(null)
    }
    transaction.commit()
}

fun FragmentManager.closeFragment() {
    popBackStack()
}