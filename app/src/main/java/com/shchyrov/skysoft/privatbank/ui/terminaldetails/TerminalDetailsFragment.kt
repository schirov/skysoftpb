package com.shchyrov.skysoft.privatbank.ui.terminaldetails

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import com.shchyrov.skysoft.privatbank.R
import com.shchyrov.skysoft.privatbank.databinding.FragmentTerminalDetailsBinding
import com.shchyrov.skysoft.privatbank.entity.Terminal

class TerminalDetailsFragment : Fragment() {

    companion object {
        private const val TERMINAL_DATA = "terminal"
        fun getBundleData(terminal: Terminal): Bundle =
            Bundle().apply { putParcelable(TERMINAL_DATA, terminal) }
    }

    private lateinit var binding: FragmentTerminalDetailsBinding
    private val viewModel = TerminalDetailsViewModel()

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_terminal_details, container, false)

        viewModel.terminal = arguments?.getParcelable(TERMINAL_DATA)!!
        binding.viewModel = viewModel

        return binding.root
    }

}