package com.shchyrov.skysoft.privatbank.di.module

import com.shchyrov.skysoft.privatbank.BuildConfig
import com.shchyrov.skysoft.privatbank.network.ApiService
import com.shchyrov.skysoft.privatbank.network.ResponseInterceptor
import dagger.Module
import dagger.Provides
import dagger.Reusable
import io.reactivex.schedulers.Schedulers
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.moshi.MoshiConverterFactory
import javax.inject.Singleton

@Module
@Suppress("unused")
object ApiNetworkModule {

    @Singleton
    @Provides
    fun provideOkHttpClient(responseInterceptor: ResponseInterceptor): OkHttpClient = OkHttpClient.Builder()
        .addInterceptor(responseInterceptor)
        .build()

    @Provides
    @Reusable
    @JvmStatic
    internal fun provideApiService(retrofit: Retrofit): ApiService {
        return retrofit.create(ApiService::class.java)
    }

    @Provides
    @Reusable
    @JvmStatic
    internal fun provideRetrofitInterface(okHttpClient: OkHttpClient): Retrofit {
        return Retrofit.Builder()
            .baseUrl(BuildConfig.BASE_URL)
            .client(okHttpClient)
            .addConverterFactory(MoshiConverterFactory.create())
            .addCallAdapterFactory(RxJava2CallAdapterFactory.createWithScheduler(Schedulers.io()))
            .build()
    }
}