package com.shchyrov.skysoft.privatbank.di

import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import androidx.room.Room
import com.shchyrov.skysoft.privatbank.model.database.AppDatabase
import com.shchyrov.skysoft.privatbank.ui.terminals.TerminalListViewModel

class ViewModelFactory(private val fragment: Fragment): ViewModelProvider.Factory{
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        if (modelClass.isAssignableFrom(TerminalListViewModel::class.java)) {
            val db = Room.databaseBuilder(fragment.context!!, AppDatabase::class.java, "Terminals.db")
                .fallbackToDestructiveMigration()
                .build()
            @Suppress("UNCHECKED_CAST")
            return TerminalListViewModel(db.terminalDao()) as T
        }
        throw IllegalArgumentException("Unknown ViewModel class")

    }
}