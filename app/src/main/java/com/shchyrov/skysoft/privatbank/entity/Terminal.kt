package com.shchyrov.skysoft.privatbank.entity

import android.os.Parcelable
import androidx.room.Entity
import androidx.room.PrimaryKey
import kotlinx.android.parcel.Parcelize

@Entity
@Parcelize
class Terminal(
    @field:PrimaryKey(autoGenerate = true)
    val id: Int,
    val type: String,
    val cityUA: String,
    val fullAddressUa: String,
    val placeUa: String,
    val latitude: Double,
    val longitude: Double,
    val tw: Schedule?
) : Parcelable {
    override fun toString(): String {
        return "Terminal(id=$id, type='$type', cityUA='$cityUA', fullAddressUa='$fullAddressUa', placeUa='$placeUa', latitude=$latitude, longitude=$longitude, schedule=$tw)"
    }
}