package com.shchyrov.skysoft.privatbank.ui.terminals

import androidx.lifecycle.MutableLiveData
import com.shchyrov.skysoft.privatbank.base.BaseViewModel
import com.shchyrov.skysoft.privatbank.entity.Terminal
import com.shchyrov.skysoft.privatbank.model.TerminalDao
import com.shchyrov.skysoft.privatbank.network.ApiService
import com.shchyrov.skysoft.privatbank.utils.listeners.OnItemClickListener
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import java.util.concurrent.TimeUnit
import javax.inject.Inject

class TerminalListViewModel(private val terminalDao: TerminalDao) : BaseViewModel(), OnItemClickListener {

    @Inject
    lateinit var apiService: ApiService
    val terminalListAdapter: TerminalListAdapter = TerminalListAdapter(this)
    private val selectedTerminal: MutableLiveData<Terminal> = MutableLiveData()

    private val compositeDisposable = CompositeDisposable()

    init {
        loadDataFromDb()
        loadDataFromServer()
    }

    override fun onCleared() {
        super.onCleared()
        compositeDisposable.clear()
    }

    override fun <T> onItemClick(item: T) {
        if (item !is Terminal)
            return
        selectedTerminal.value = item
    }

    fun getSelectedTerminal(): MutableLiveData<Terminal> = selectedTerminal

    fun setSelectedTerminal(terminal: Terminal?) {
        selectedTerminal.value = terminal
    }

    private fun loadDataFromDb() {
        terminalDao.getTerminals().toObservable()
            .debounce(400, TimeUnit.MILLISECONDS)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe { result -> onRetrieveListSuccess(result, false) }
            .let { compositeDisposable.add(it) }
    }

    private fun loadDataFromServer() {
        apiService.getTerminals()
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({ result -> onRetrieveListSuccess(result) }, { e -> onRetrieveListError(e) })
            .let { compositeDisposable.add(it) }
    }

    private fun onRetrieveListSuccess(terminalList: List<Terminal>, fromAPI: Boolean = true) {
        terminalListAdapter.updateList(terminalList)
        if (!fromAPI)
            return
        Observable.fromCallable {
            terminalDao.clearAll()
            terminalDao.insertAll(*terminalList.toTypedArray())
        }
            .debounce(1000, TimeUnit.MILLISECONDS)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({
            }, { e -> e.printStackTrace() }).let { compositeDisposable.add(it) }
    }

    private fun onRetrieveListError(e: Throwable) {
        e.printStackTrace()
    }

}