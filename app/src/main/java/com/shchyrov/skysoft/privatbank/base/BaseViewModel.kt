package com.shchyrov.skysoft.privatbank.base

import androidx.lifecycle.ViewModel
import com.shchyrov.skysoft.privatbank.di.component.DaggerViewModelInjector
import com.shchyrov.skysoft.privatbank.di.component.ViewModelInjector
import com.shchyrov.skysoft.privatbank.di.module.ApiNetworkModule
import com.shchyrov.skysoft.privatbank.ui.terminaldetails.TerminalDetailsViewModel
import com.shchyrov.skysoft.privatbank.ui.terminals.TerminalListViewModel
import com.shchyrov.skysoft.privatbank.ui.terminals.TerminalViewModel

abstract class BaseViewModel : ViewModel() {
    private val injector: ViewModelInjector = DaggerViewModelInjector
        .builder()
        .networkModule(ApiNetworkModule)
        .build()

    init {
        inject()
    }

    private fun inject() {
        when (this) {
            is TerminalListViewModel -> injector.inject(this)
            is TerminalViewModel -> injector.inject(this)
            is TerminalDetailsViewModel -> injector.inject(this)
        }
    }
}