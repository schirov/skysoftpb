package com.shchyrov.skysoft.privatbank.model.database

import androidx.room.TypeConverter
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import com.shchyrov.skysoft.privatbank.entity.Schedule

class Converters {
    @TypeConverter
    fun fromString(value: String?): Schedule? {
        return Gson().fromJson(value, object : TypeToken<Schedule>() {}.type)
    }

    @TypeConverter
    fun fromSchedule(value: Schedule?): String? {
        return Gson().toJson(value)
    }
}