package com.shchyrov.skysoft.privatbank.network

import android.util.Log
import com.google.gson.Gson
import com.shchyrov.skysoft.privatbank.utils.NETWORK_REQUEST_LOG_TAG
import okhttp3.Interceptor
import okhttp3.Response
import okhttp3.ResponseBody
import org.json.JSONObject
import javax.inject.Inject

class ResponseInterceptor @Inject constructor() : Interceptor {

    companion object {
        private val DEVICES = "devices"
    }

    override fun intercept(chain: Interceptor.Chain): Response {
        val response = chain.proceed(chain.request())
        val json = JSONObject(response.body()?.string())

        Log.d(NETWORK_REQUEST_LOG_TAG, "${response.request().url()} $json")

        var result = json.toString()
        if (json.has(DEVICES)) {
            result = json.get(DEVICES).toString()
        }

        return response.newBuilder()
            .body(ResponseBody.create(response.body()?.contentType(), result))
            .build()
    }
}