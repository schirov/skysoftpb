package com.shchyrov.skysoft.privatbank.ui.terminals

import android.util.Log
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.shchyrov.skysoft.privatbank.R
import com.shchyrov.skysoft.privatbank.databinding.ItemTerminalBinding
import com.shchyrov.skysoft.privatbank.entity.Terminal
import com.shchyrov.skysoft.privatbank.utils.listeners.OnItemClickListener

class TerminalListAdapter(private val listener: OnItemClickListener) : RecyclerView.Adapter<TerminalListAdapter.ViewHolder>() {

    private lateinit var terminalList: List<Terminal>

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): TerminalListAdapter.ViewHolder {
        val binding: ItemTerminalBinding =
            DataBindingUtil.inflate(LayoutInflater.from(parent.context), R.layout.item_terminal, parent, false)
        return ViewHolder(binding)
    }

    override fun onBindViewHolder(holder: TerminalListAdapter.ViewHolder, position: Int) {
        holder.bind(terminalList[position])
    }

    override fun getItemCount(): Int {
        return if (::terminalList.isInitialized) terminalList.size else 0
    }

    fun updateList(terminalList: List<Terminal>) {
        this.terminalList = terminalList
        notifyDataSetChanged()
    }

    inner class ViewHolder(private val binding: ItemTerminalBinding) : RecyclerView.ViewHolder(binding.root) {
        private val viewModel = TerminalViewModel()

        fun bind(terminal: Terminal) {
            viewModel.bind(terminal)
            viewModel.setOnClickListener(listener)
            binding.viewModel = viewModel
        }
    }
}