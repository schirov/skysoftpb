package com.shchyrov.skysoft.privatbank

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.shchyrov.skysoft.privatbank.ui.terminals.TerminalListFragment

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        supportFragmentManager.beginTransaction()
            .replace(R.id.container_main, TerminalListFragment())
            .commit()

    }
}
