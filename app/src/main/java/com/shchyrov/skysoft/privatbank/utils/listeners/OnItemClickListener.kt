package com.shchyrov.skysoft.privatbank.utils.listeners

interface OnItemClickListener {
    fun <T> onItemClick(item: T)
}