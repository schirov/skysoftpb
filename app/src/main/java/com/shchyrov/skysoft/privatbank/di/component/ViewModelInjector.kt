package com.shchyrov.skysoft.privatbank.di.component

import com.shchyrov.skysoft.privatbank.di.module.ApiNetworkModule
import com.shchyrov.skysoft.privatbank.ui.terminaldetails.TerminalDetailsViewModel
import com.shchyrov.skysoft.privatbank.ui.terminals.TerminalListViewModel
import com.shchyrov.skysoft.privatbank.ui.terminals.TerminalViewModel
import dagger.Component
import javax.inject.Singleton

@Singleton
@Component(modules = [(ApiNetworkModule::class)])
interface ViewModelInjector {

    fun inject(terminalListViewModel: TerminalListViewModel)

    fun inject(terminalViewModel: TerminalViewModel)

    fun inject(terminalDetailsViewModel: TerminalDetailsViewModel)

    @Component.Builder
    interface Builder {
        fun build(): ViewModelInjector

        fun networkModule(networkModule: ApiNetworkModule): Builder
    }
}