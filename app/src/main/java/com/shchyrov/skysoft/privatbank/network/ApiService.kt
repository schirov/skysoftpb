package com.shchyrov.skysoft.privatbank.network

import com.shchyrov.skysoft.privatbank.entity.Terminal
import io.reactivex.Observable
import retrofit2.http.GET

interface ApiService {

    @GET("infrastructure?json&tso&city=Вінниця")
    fun getTerminals(): Observable<List<Terminal>>
}